<%-- 
    Document   : agregarRestaurante.jsp
    Created on : 8/02/2021, 12:26:26 PM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar restaurante ${restaurante.nombre}</title>
    </head>
    <body>
        <div class="container">
    
            <div class="bs-docs-section clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bs-component">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">

                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li class="nav-item">
                                                <a href="index.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    Restaurantes <span class="bx bx-home"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="agregarRestaurante.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    Agregar Restaurante <span class="bx bx-home"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">    
                <form action="editarMenu.htm?menu=${menu.menuId}" method="post">
                    <div class="row col-sm-4">
                        <label>nombre</label>
                        <input class="form-control" type="text" name="nombre" value="${menu.nombre}" class="" required>
                    </div>
                    <div class="row col-sm-4">
                        <label>tipo</label>
                        <select class="form-control" name="tipoMenu" required>
                            <option id="option_o" value="">----</option>
                            <option id="option_entrada" value=${entrada}>Entrada</option>
                            <option id="option_bebidas" value=${bebidas}>Bebidas</option>
                            <option id="option_fuerte" value=${plato_fuerte}>Plato fuerte</option>
                            <option id="option_postre" value=${postre}>Postre</option>
                        </select>
                    </div>
                    <div class="row col-sm-4">    
                        <label>Precio</label>
                        <input class="form-control" type="number" name="precio" value="${menu.precio}" class="" required>
                    </div>
                        <br><br>
                    <div class="row col-sm-12">
                        <button type="submit" class="btn-success">Guardar</button>
                    </div>    
                </form>

            </div>
        </div>
    </body>
    
    <script type="text/javascript">
       
    if("${menu.tipoMenu}"=="${entrada}"){
         document.getElementById("option_entrada").selected = true;
    }else if("${menu.tipoMenu}"=="${bebidas}"){
        document.getElementById("option_bebidas").selected = true;
    }else if("${menu.tipoMenu}"=="${plato_fuerte}"){
        document.getElementById("option_fuerte").selected = true;
    }else if("${menu.tipoMenu}"=="${postre}"){
        document.getElementById("option_postre").selected = true;
    }
    
    </script>
</html>
