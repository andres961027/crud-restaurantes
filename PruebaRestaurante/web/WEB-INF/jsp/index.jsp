<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Restaurantes</title>
    </head>

    <body>
    <div class="container">
    
    <div class="bs-docs-section clearfix">
        <div class="row">
            <div class="col-lg-12">
                <div class="bs-component">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                         <ul class="nav navbar-nav">
                            <li class="dropdown">
                              <a href="" class="bx bx-home" data-toggle="dropdown" role="button" aria-expanded="false">
                                  Restaurantes <i class="bx bx-user mr-50"></i>
                              </a>
                              
                            </li>
                            <li class="nav-item">
                                <a href="agregarRestaurante.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Agregar Restaurante <span class="bx bx-home"></i>
                                </a>
                            </li>
                         </ul>

                        </div>
                    </div>
                </nav>
            </div>
              </div>
          </div>
      </div>
    </div>  

        
        <div class="container ">
            <table border="1" class=" table table-condensed">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>razon social</th>
                        <th>nombre</th>
                        <th>Tipo</th>
                        <th>Hora de apertura</th>
                        <th>Hora de cierre</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="restaurante" items="${lista}">
                        <tr>
                            <td>${restaurante.restauranteId}</td>
                            <td>${restaurante.razonSocial}</td>
                            <td>${restaurante.nombre}</td>
                            <td>${restaurante.tipoRestaurante}</td>
                            <td>${restaurante.horaApertura}</td>
                            <td>${restaurante.horaCierre}</td>  
                            <td>
                                <a class="btn btn-success" href="editarRestaurante.htm?restaurante=${restaurante.restauranteId}">Editar</a>
                                <a class="btn btn-info" href="detallesRestaurante.htm?restaurante=${restaurante.restauranteId}">Detalles</a>
                                <a class="btn btn-danger" href="eliminarRestaurante.htm?restaurante=${restaurante.restauranteId}">Eliminar</a>
                            </td>  
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>        
        
    </body>
</html>
