/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.HashMap;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import model.Ingrediente;
import model.Menu;
import model.Restaurante;
import model.TipoMenu;
import model.TipoRestaurante;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import persistence.Persistence;

/**
 *
 * @author root
 */

@org.springframework.stereotype.Controller
public class Controller {
    private Persistence persistence = new Persistence();
    ModelAndView mav = new ModelAndView();
    
    /***
     * Crud de Restaurantes
     * 
     */
    @RequestMapping("index.htm")
    public ModelAndView listarRestaurantes(){
        this.mav.addObject("lista",persistence.getRestaurantes());
        this.mav.setViewName("index");
        return this.mav;
    }
    
    @RequestMapping(value = "agregarRestaurante.htm", method = RequestMethod.GET)
    public ModelAndView agregarRestaurante(){
        this.mav.addObject("vegano",TipoRestaurante.VEGANO);
        this.mav.addObject("vegetariano",TipoRestaurante.VEGETARIANO);
        this.mav.addObject("carnes",TipoRestaurante.CARNESROJAS_Y_AVES);
        this.mav.addObject("pescados",TipoRestaurante.PESCADOS_MARISCOS);
        this.mav.setViewName("agregarRestaurante");
        return this.mav;
    }
    
    @RequestMapping(value = "agregarRestaurante.htm", method = RequestMethod.POST)
    public ModelAndView agregarRestaurante(Restaurante restaurante){
        persistence.guardarRestaurante(restaurante);
        return new ModelAndView("redirect:/index.htm");
    }
    
    
    @RequestMapping(value = "editarRestaurante.htm", method = RequestMethod.GET)
    public ModelAndView editarRestaurante(HttpServletRequest request){
        Restaurante restaurante = persistence.getRestaurante(Long.parseLong(request.getParameter("restaurante")));
        
        this.mav.addObject("restaurante",restaurante);
        this.mav.addObject("vegano",TipoRestaurante.VEGANO);
        this.mav.addObject("vegetariano",TipoRestaurante.VEGETARIANO);
        this.mav.addObject("carnes",TipoRestaurante.CARNESROJAS_Y_AVES);
        this.mav.addObject("pescados",TipoRestaurante.PESCADOS_MARISCOS);
        this.mav.setViewName("editarRestaurante");
        return this.mav;
    }
    
    @RequestMapping(value = "editarRestaurante.htm", method = RequestMethod.POST)
    public ModelAndView editarRestaurante(Restaurante restaurante, HttpServletRequest request){
        persistence.updateRestaurante(restaurante,Long.parseLong(request.getParameter("restaurante")));
        return new ModelAndView("redirect:/index.htm");
    }
    
    @RequestMapping(value = "eliminarRestaurante.htm")
    public ModelAndView eliminarRestaurante(HttpServletRequest request){
        persistence.eliminarRestaurante(Long.parseLong(request.getParameter("restaurante")));
        return new ModelAndView("redirect:/index.htm");
    }
    
    /***
     * Crud de Menus
     * 
     */
    
    @RequestMapping(value = "detallesRestaurante.htm")
    public ModelAndView detallesRestaurante(HttpServletRequest request){
        this.restaurante=0;
        Restaurante restaurante = persistence.getRestaurante(Long.parseLong(request.getParameter("restaurante")));
        
        this.mav.addObject("lista", restaurante.getMenus());
        this.mav.addObject("restaurante", restaurante);
        this.mav.addObject("size", restaurante.getMenus().size());
        this.mav.setViewName("detallesRestaurante");
        return this.mav;
    }
    
    private long restaurante =0;
    @RequestMapping(value = "agregarMenu.htm", method = RequestMethod.GET)
    public ModelAndView agregarMenu(HttpServletRequest request){
        this.restaurante = Long.parseLong(request.getParameter("restaurante"));
        this.mav.addObject("entrada",TipoMenu.ENTRADA);
        this.mav.addObject("bebidas",TipoMenu.BEBIDAS);
        this.mav.addObject("plato_fuerte",TipoMenu.PLATO_FUERTE);
        this.mav.addObject("postre",TipoMenu.POSTRES);
        this.mav.addObject("restaurante",restaurante);
        this.mav.setViewName("agregarMenu");
        return this.mav;
    }
    @RequestMapping(value = "agregarMenu.htm", method = RequestMethod.POST)
    public ModelAndView agregarMenu(Menu menu, HttpServletRequest request){
        persistence.guardarMenu(menu, this.restaurante);
        return new ModelAndView("redirect:/detallesRestaurante.htm?restaurante="+this.restaurante);
    }
    
    @RequestMapping(value = "editarMenu.htm", method = RequestMethod.GET)
    public ModelAndView editarMenu(HttpServletRequest request){
        Menu menu = persistence.getMenu(Long.parseLong(request.getParameter("menu")));
        this.mav.addObject("menu",menu);
        this.mav.addObject("entrada",TipoMenu.ENTRADA);
        this.mav.addObject("bebidas",TipoMenu.BEBIDAS);
        this.mav.addObject("plato_fuerte",TipoMenu.PLATO_FUERTE);
        this.mav.addObject("postre",TipoMenu.POSTRES);
        this.mav.setViewName("editarMenu");
        return this.mav;
    }
    
    @RequestMapping(value = "editarMenu.htm", method = RequestMethod.POST)
    public ModelAndView editarMenu(Menu menu, HttpServletRequest request){
        long restauranteId= persistence.updateMenu(menu, Long.parseLong(request.getParameter("menu")));
        return new ModelAndView("redirect:/detallesRestaurante.htm?restaurante="+restauranteId);
    }
    
    @RequestMapping(value = "eliminarMenu.htm")
    public ModelAndView eliminarMenu(HttpServletRequest request){
        long restauranteId=persistence.eliminarMenu(Long.parseLong(request.getParameter("menu")));
        return new ModelAndView("redirect:/detallesRestaurante.htm?restaurante="+restauranteId);
    }
    
    /***
     * Crud de Ingredientes
     * 
     */
    @RequestMapping(value = "detallesMenu.htm")
    public ModelAndView detallesMenu(HttpServletRequest request){
        this.menu=0;
        Menu menu = persistence.getMenu(Long.parseLong(request.getParameter("menu")));
        this.mav.addObject("lista", menu.getIngredientes());
        this.mav.addObject("menu", menu);
        this.mav.setViewName("detallesMenu");
        return this.mav;
    }
    
    private long menu =0;
    @RequestMapping(value = "agregarIngrediente.htm", method = RequestMethod.GET)
    public ModelAndView agregarIngrediente(HttpServletRequest request){
        this.menu = Long.parseLong(request.getParameter("menu"));
        this.mav.setViewName("agregarIngrediente");
        return this.mav;
    }
    @RequestMapping(value = "agregarIngrediente.htm", method = RequestMethod.POST)
    public ModelAndView agregarIngrediente(Ingrediente ingrediente, HttpServletRequest request){
        persistence.guardarIngrediente(ingrediente, this.menu);
        return new ModelAndView("redirect:/detallesMenu.htm?menu="+this.menu);
    }
    
    @RequestMapping(value = "editarIngrediente.htm", method = RequestMethod.GET)
    public ModelAndView editarIngrediente(HttpServletRequest request){
        this.menu = Long.parseLong(request.getParameter("ingrediente"));
        Ingrediente ingrediente = persistence.getIngrediente(Long.parseLong(request.getParameter("ingrediente")));
        this.mav.addObject("ingrediente",ingrediente);
        this.mav.setViewName("editarIngrediente");
        return this.mav;
    }
    
    @RequestMapping(value = "editarIngrediente.htm", method = RequestMethod.POST)
    public ModelAndView editarIngrediente(Ingrediente ingrediente, HttpServletRequest request){
        long menuId= persistence.updateIngrediente(ingrediente, Long.parseLong(request.getParameter("ingrediente")));
        return new ModelAndView("redirect:/detallesMenu.htm?menu="+menuId);
    }
    
    
    @RequestMapping(value = "eliminarIngrediente.htm")
    public ModelAndView eliminarIngrediente(HttpServletRequest request){
        long menuId=persistence.eliminarIngrediente(Long.parseLong(request.getParameter("ingrediente")));
        return new ModelAndView("redirect:/detallesMenu.htm?menu="+menuId);
    }
}