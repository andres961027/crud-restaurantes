/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.util.List;
import java.util.Set;
import model.Ingrediente;
import model.Menu;
import model.Restaurante;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import util.HibernateUtil;

/**
 *
 * @author root
 */
public class Persistence {
    /**
     * Guardados  de objetos
     * @param restaurante 
     */
    public void guardarRestaurante(Restaurante restaurante){
        restaurante.setRestauranteId(getConsecutivoRestaurante());
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(restaurante);
        transaction.commit();
        session.close();
    }
    
    public void guardarMenu(Menu menu, long restaurante){
        menu.setMenuId(getConsecutivoMenu());
        menu.setRestaurante(getRestaurante(restaurante));
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(menu);
        transaction.commit();
        session.close();
    }
    
    public void guardarIngrediente(Ingrediente ingrediente, long id){
        ingrediente.setIngredienteId(getConsecutivoIngrediente()); 
        ingrediente.setMenu(getMenu(id));
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(ingrediente);
        transaction.commit();
        session.close();
    }
    
    /**
     * Listados
     * @return 
     */
    
    public List getRestaurantes(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        List<Restaurante> list = session.createQuery("from Restaurante").list();
        transaction.commit();
        session.close();
        return list;
    }
    public List getMenus(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        List<Menu> list = session.createQuery("from Menu").list();
        transaction.commit();
        session.close();
        return list;
    }
    public List getIngredientes(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        List<Menu> list = session.createQuery("from Ingrediente").list();
        transaction.commit();
        session.close();
        return list;
    }
    
    /**
     * Obtener
     * @param restauranteId
     * @return 
     */
    public Restaurante getRestaurante(long restauranteId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Restaurante restaurante = (Restaurante) session.get(Restaurante.class,restauranteId); 
        transaction.commit();
        session.close();
        return restaurante;
    }
    
    public Menu getMenu(long menuId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Menu  menu  = (Menu) session.get(Menu.class,menuId); 
        transaction.commit();
        session.close();
        return menu;
    }
    
    public Ingrediente getIngrediente(long ingredienteId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Ingrediente  ingrediente  = (Ingrediente) session.get(Ingrediente.class,ingredienteId); 
        transaction.commit();
        session.close();
        return ingrediente;
    }
    /**
     * Update
     * 
     */
    public void updateRestaurante(Restaurante restaurante,long idRestaurante ){
        Restaurante restauranteEditar = getRestaurante(idRestaurante);
        restauranteEditar.setRazonSocial(restaurante.getRazonSocial());
        restauranteEditar.setNombre(restaurante.getNombre());
        restauranteEditar.setTipoRestaurante(restaurante.getTipoRestaurante());
        restauranteEditar.setCiudad(restaurante.getCiudad());
        restauranteEditar.setHoraApertura(restaurante.getHoraApertura());
        restauranteEditar.setHoraCierre(restaurante.getHoraCierre());
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(restauranteEditar);
        transaction.commit();
        session.close();
    }
    
    public long updateMenu(Menu menu,long idMenu ){
        Menu menuEditar = getMenu(idMenu);
        long salida=menuEditar.getRestaurante().getRestauranteId();
        menuEditar.setNombre(menu.getNombre());
        menuEditar.setTipoMenu(menu.getTipoMenu());
        menuEditar.setPrecio(menu.getPrecio());
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(menuEditar);
        transaction.commit();
        session.close();
        
        return salida;
    }
    public long updateIngrediente(Ingrediente ingrediente,long idingrediente ){
        Ingrediente ingredienteEditar = getIngrediente(idingrediente);
        long salida=ingredienteEditar.getMenu().getMenuId();
        ingredienteEditar.setNombre(ingrediente.getNombre());
        ingredienteEditar.setCalorias(ingrediente.getCalorias());
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(ingredienteEditar);
        transaction.commit();
        session.close();
        return salida;
    }
    
    /**
     * Eliminaciones
     * @param parseLong 
     */
    
    public void eliminarRestaurante(long id) {
        Restaurante restaurante = getRestaurante(id);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(restaurante);
        transaction.commit();
        session.close();
    }

    public long eliminarMenu(long id) {
        Menu menu = getMenu(id);
        long salida = menu.getRestaurante().getRestauranteId();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(menu);
        transaction.commit();
        session.close();
        return salida;
    }
    
    public long eliminarIngrediente(long id) {
        Ingrediente ingrediente = getIngrediente(id);
        long salida = ingrediente.getMenu().getMenuId();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(ingrediente);
        transaction.commit();
        session.close();
        return salida;
    }
    
    /**
     * Get max ids
     */
    
    
    public long getConsecutivoMenu(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        
        Menu menu = 
        (Menu) session.createCriteria(Menu.class)
        .addOrder(Order.desc("menuId"))
        .setMaxResults(1)
        .uniqueResult();
        transaction.commit();
        session.close();
        
     return menu==null?1:menu.getMenuId() +1;
    }
    public long getConsecutivoIngrediente(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        
        Ingrediente ingrediente = 
        (Ingrediente) session.createCriteria(Ingrediente.class)
        .addOrder(Order.desc("ingredienteId"))
        .setMaxResults(1)
        .uniqueResult();
        transaction.commit();
        session.close();
        
     return ingrediente==null?1:ingrediente.getIngredienteId() +1;
    }
    public long getConsecutivoRestaurante(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Restaurante restaurante = 
        (Restaurante) session.createCriteria(Restaurante.class)
        .addOrder(Order.desc("restauranteId"))
        .setMaxResults(1)
        .uniqueResult();
        transaction.commit();
        session.close();
        
     return restaurante==null?1:restaurante.getRestauranteId() +1;
    }
    
}
