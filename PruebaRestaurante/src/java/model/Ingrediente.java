/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Table
@Entity(name = "ingredientes")
public class Ingrediente implements java.io.Serializable{
    
    @Id
    @Column(name = "ingrediente_id")
    private long ingredienteId;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "calorias")
    private Integer calorias;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name= "menu_id")
    private Menu menu;
    
    public Ingrediente(long ingredienteId, String nombre, Integer calorias, Menu menu) {
        this.ingredienteId = ingredienteId;
        this.nombre = nombre;
        this.calorias = calorias;
        this.menu=menu;
    }
    
    public Ingrediente (){
    
    }
    
    public long getIngredienteId() {
        return ingredienteId;
    }

    public void setIngredienteId(long ingredienteId) {
        this.ingredienteId = ingredienteId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCalorias() {
        return calorias;
    }

    public void setCalorias(Integer calorias) {
        this.calorias = calorias;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Menu getMenu() {
        return menu;
    }
    
}
