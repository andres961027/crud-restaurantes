/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Table
@Entity(name="Menus")
public class Menu implements java.io.Serializable{
    
    @Id
    @Column(name="menu_id")
    private long menuId;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name="tipo_menu")
    private TipoMenu tipoMenu;
    
    @Column(name="nombre")
    private String nombre;
    
    @OneToMany(mappedBy = "menu",cascade = CascadeType.ALL, fetch = FetchType.EAGER) 
    private Set<Ingrediente> ingredientes = new HashSet(0);
    
    @Column(name="precio")
    private Double precio;
    
    @ManyToOne(cascade = CascadeType.MERGE )
    @JoinColumn(name= "restaurante_id")
    private Restaurante restaurante;
    
    public Menu(long menuId, TipoMenu tipoMenu, String nombre, Double precio, Restaurante restaurante) {
        this.menuId = menuId;
        this.tipoMenu = tipoMenu;
        this.nombre = nombre;
        this.precio = precio;
        this.restaurante=restaurante;
    }
    public Menu(){}

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public TipoMenu getTipoMenu() {
        return tipoMenu;
    }

    public void setTipoMenu(TipoMenu tipoMenu) {
        this.tipoMenu = tipoMenu;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public Set<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(Set<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }
    
}
