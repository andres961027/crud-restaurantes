/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Entity
@Table(name="Restaurantes")
public class Restaurante implements java.io.Serializable{

    @Id
    @Column(name = "restaurante_id")
    private long restauranteId;
    @Column(name = "razon_social")
    private String razonSocial; 
    @Column(name = "nombre")
    private String nombre; 
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name="tipo_restaurante")
    private TipoRestaurante tipoRestaurante;
    
    @Column(name = "ciudad")
    private String ciudad; 
    
    @Column(name = "hora_apertura")
    private String horaApertura; 
    
    @Column(name = "hora_cierre")
    private String horaCierre; 
    
    @OneToMany(mappedBy = "restaurante",cascade = CascadeType.ALL, fetch = FetchType.EAGER) 
    private Set<Menu> menus = new HashSet(0); 
    
    public long getRestauranteId() {
        return restauranteId;
    }

    public void setRestauranteId(long restauranteId) {
        this.restauranteId = restauranteId;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoRestaurante getTipoRestaurante() {
        return tipoRestaurante;
    }

    public void setTipoRestaurante(TipoRestaurante tipoRestaurante) {
        this.tipoRestaurante = tipoRestaurante;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public String getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(String horaCierre) {
        this.horaCierre = horaCierre;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public Restaurante(long restauranteId, String razonSocial, String nombre, TipoRestaurante tipoRestaurante, String ciudad, String horaApertura, String horaCierre) {
        this.restauranteId = restauranteId;
        this.razonSocial = razonSocial;
        this.nombre = nombre;
        this.tipoRestaurante = tipoRestaurante;
        this.ciudad = ciudad;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
    }
    public Restaurante(){}
    
}
