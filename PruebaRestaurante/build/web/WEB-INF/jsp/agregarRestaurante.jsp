<%-- 
    Document   : agregarRestaurante.jsp
    Created on : 8/02/2021, 12:26:26 PM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Restaurante</title>
    </head>
    <body>
        <div class="container">
    
            <div class="bs-docs-section clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bs-component">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">

                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li class="nav-item">
                                                <a href="index.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    Restaurantes <span class="bx bx-home"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="agregarRestaurante.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    Agregar Restaurante <span class="bx bx-home"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">    
                <form action="" method="POST">
                    <div class="row col-sm-4">
                        <label>Razon social</label>
                        <input class="form-control" type="text" name="razonSocial" class="" required>
                    </div>
                    <div class="row col-sm-4">
                        <label>nombre</label>
                        <input class="form-control" type="text" name="nombre" class="" required>
                    </div>
                    <div class="row col-sm-4">
                        <label>tipo</label>
                        <select class="form-control" name="tipoRestaurante" required>
                            <option id="option_o" value="">----</option>
                            <option id="option_vegano" value=${vegano}>Vegano</option>
                            <option id="option_vegetariano" value=${vegetariano}>Vegetariano</option>
                            <option id="option_carnes" value=${carnes}>Carnes rojas y aves</option>
                            <option id="option_pescados" value=${pescados}>Pescados y mariscos</option>
                        </select>
                    </div>
                    <div class="row col-sm-4">    
                        <label>Ciudad</label>
                        <input class="form-control" type="text" name="ciudad" class="" required>
                    </div>
                    <div class="row col-sm-4">
                        <label>hora de apertura</label>
                        <input class="form-control" type="text" name="horaApertura" class="" required>
                    </div>
                    <div class="row col-sm-4">
                        <label>hora de cierre</label>
                        <input class="form-control" type="text" name="horaCierre" class="" required>
                    </div>
                        <br><br>
                    <div class="row col-sm-12">
                        <button type="submit" class="btn-success">Guardar</button>
                    </div>    
                </form>

            </div>
        </div>
        
    </body>
</html>
