<%-- 
    Document   : detallesRestaurante.jsp
    Created on : 8/02/2021, 09:58:34 PM
    Author     : root
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detalles Menu </title>
    </head>

    <body>
    <div class="container">
    
    <div class="bs-docs-section clearfix">
        <div class="row">
            <div class="col-lg-12">
                <div class="bs-component">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                         <ul class="nav navbar-nav">
                            <li class="dropdown">
                              <a href="index.htm" class="bx bx-home" data-toggle="dropdown" role="button" aria-expanded="false">
                                  Restaurantes <i class="bx bx-user mr-50"></i>
                              </a>
                              
                            </li>
                            <li class="nav-item">
                                <a href="agregarRestaurante.htm" class="single-menu nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Agregar Restaurante <span class="bx bx-home"></i>
                                </a>
                            </li>
                         </ul>

                        </div>
                    </div>
                </nav>
            </div>
              </div>
          </div>
      </div>
    </div>  

        
        <div class="container ">
            <table border="1" class=" table table-condensed">
                <h2>Listado de menus para el restaurante: ${menu.nombre}
                <a class="btn btn-success pull-right" href="agregarIngrediente.htm?menu=${menu.menuId}">Agregar Ingrediente</a>
                </h2>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Calorias</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="ingrediente" items="${lista}">
                        <tr>
                            <td>${ingrediente.ingredienteId}</td>
                            <td>${ingrediente.nombre}</td>
                            <td>${ingrediente.calorias}</td>  
                            <td>
                                <a class="btn btn-success" href="editarIngrediente.htm?ingrediente=${ingrediente.ingredienteId}">Editar</a>
                                <a class="btn btn-danger" href="eliminarIngrediente.htm?ingrediente=${ingrediente.ingredienteId}">Eliminar ingrediente</a>
                            </td>  
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>        
        
    </body>
</html>
